const express = require("express");
//Mongoose is an ODM library to let our ExpressJS API manipulate a mongoDB database
const mongoose = require("mongoose");
const app = express();
const port = 4000;

/*
	Mongoose Connection

	mongoose.connect() is a method to connect our api with our mongodb database via the user of mongoose. It has 2 arguments. First, is the connection string to connect our api to our mongodb. Second, is an object used to add information between mongoose and mongodb.

	replace/change <password> in the connection string to your db user password

	just before the ? in the connection string, add the database name.
*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.0t9omqh.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true

});
//We will create notifications if the connection to the db is a success or failed.
let db = mongoose.connection;
//This is to show notifications if an error is mongoDB related or not
db.on('error',console.error.bind(console, "MongoDB Connection Error."));
//This is to show if the application is connected to mongoDB
db.once('open',()=>console.log("Connected to MongoDB"));


app.use(express.json());

//import our routes and use it as middleware.
//Which means, that we will be able to group our routes
const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes);

//use our routes and group them together under '/courses'
//our endpoints
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);


app.listen(port,()=>console.log(`Server is running at port ${port}`));