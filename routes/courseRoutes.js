/*
	To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

	the Router() method, will contain our routes
*/

const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const { verify,verifyAdmin } = auth;

//All routes to courses now has an endpoint prefaced with /courses
//endpoint - /courses/
router.get('/',courseControllers.getAllCourses);

router.post('/',verify,verifyAdmin,courseControllers.addCourse);

//get all active courses - (regular, non-logged in)
router.get("/activeCourses",courseControllers.getActiveCourses);

//we can pass data in a route without the use of request body by passing a small amount of data through the url with the use of route params
//http://localhost:4000/courses/getSingleCourse/<insert_id_here>
router.get('/getSingleCourse/:courseId',courseControllers.getSingleCourse);

//update a single course
//pass the id of the course we want to update via route params
//the update details will be passed via request body.
router.put('/updateCourse/:courseId',courseControllers.updateCourse);

//archive a single course
//pass the id of the course we want to update via route params
//we will directly update the course as inactive
router.delete('/archiveCourse/:courseId',verify,verifyAdmin,courseControllers.archiveCourse);


module.exports = router;