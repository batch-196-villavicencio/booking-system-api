const express = require("express");
const router = express.Router();

//In fact, routes should just contain the endpoints and it should only trigger the function but it should not be where we define the logic of our function.

//The business logic of our API should be in controllers.
//import our user controllers:
const userControllers = require("../controllers/userControllers");

//import auth to be able to have access and use the verify methods to act as middleware for our routes.
//Middlewares add in the route such as verify() will have access to the req,res objects.

const auth = require("../auth");

//destructure auth to get only our methods and save it in variables:
const {verify} = auth;

/*
	Updated Route Syntax:

	method("/endpoint",handlerFunction)
*/

//register
router.post('/',userControllers.registerUser);
//searchById
router.get('/details',verify,userControllers.getUserDetails);
//Route for User Authentication
router.post('/login',userControllers.loginUser);

router.post('/checkEmail',userControllers.checkEmail);

//User Enrollment
//CcourseId will come from the req.body
//userId will come from the req.user
router.post('/enroll',verify,userControllers.enroll);

module.exports = router;